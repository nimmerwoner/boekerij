# Boekerĳ

Boekerĳ intends to be an ideal ebook manager for Gnome. It builds on Gnome technologies like Gtk, Gio and Glib and implements as much as possible the HIG.

The official website resides at [the nimmerfort](http://nimmerfort.eu/boekerĳ/), where the source and a flatpak are available. 

This bitbucket provides access to the git repository and the issue tracker. 

## Installation instructions

Installation currently is done best through Flatpak. Follow the instructions outlined below, which are also mentioned on the official [site](http://nimmerfort.eu/boekerĳ/).

1. `flatpak install gnome org.gnome.Platform//3.22`
2. `flatpak --no-gpg-verify remote-add boekerĳ http://nimmerfort.eu/boekerĳ/flatpak`
3. `flatpak install boekerĳ eu.nimmerfort.Boekerij`

## Build instructions

Boekerĳ is implemented in Haskell using the [haskell-gi](https://github.com/haskell-gi/haskell-gi) bindings and the [Stack platform](https://docs.haskellstack.org/en/stable/README/). 

As such, to build Boekerĳ is simple:

1. `git clone https://bitbucket.org/nimmerwoner/boekerij.git Boekerĳ`
2. `cd Boekerĳ && stack setup && stack build`
3. `stack exec boekerĳ`

It will then start to install the appropriate dependencies and build. It can be run from the project root with `stack exec boekerĳ`, but not from anywhere else yet!

To then install it run `stack install`. This installation method is, however, at the moment not recommended, because of the above deficiency. Installing and running is easier and more reliable through the flatpak method outlined above.

## Compared to competitors

* Calibre
* Bookworm
* GNOME Books
* FBReader
* Xreader
* Easy eBook Viewer
* Atril

Good ebook support seems to be a real unfilled gap in the Linux ecosystem. Currently, the most functional and most-recommended application is Calibre, which supports reading, transferring, converting and editing ebooks. Functionally it is the best program out there. The fact that people keep developing alternatives indicates that it doesn't completely satisfy however. 

Just in the GTK world there exist Xreader, Atril, Easy eBook Viewer, which all seem to primarily focus on reading ebooks. To me, Easy eBook Viewer, is the most promising and beautiful, but it is not officially released yet. The other two are, but their ebook reading is unreliable, because books are often opened only partially or they crash. 

In this category of reader, FBReader is the most reliable and succesful, since it works well, performs best and is very fully featured. Unfortunately it doesn't integrate well with many desktop environments. In GNOME for example, the top bar buttons quite unattractive and it doesn't use typical GTK widgets.

FBReader also fits in the management  category, together with GNOME Books and Bookworm. Bookworm is however not a native GTK/GNOME app; GNOME Books does not seem to get much support, and FBReader's display of the book collection isn't very beautiful. GNOME Books' is most beautiful but aside from lacking much development, it does not support book transfering. 

To return to Calibre, this is currently the application with best management and transfering features, yet just like FBReader it doesn't integrate well with GNOME. Furthermore, it can be quite unperformant, and in my personal opinion, confusing to use.

Boekerĳ's goal then is to fill the gap of a GNOME program to organize, transfer, and edit ebooks. Reading can hopefully eventually be outsourced to another small and performant application, for example Easy eBook Viewer.
