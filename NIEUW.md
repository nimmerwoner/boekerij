# Nieuw in Boekerĳ

## 0.2

1. **Meldingen:** Boekerĳ geeft nu meldingen in het meldingenpaneel rechtsboven
   in twee situaties. Enerzijds wanneer het mislukt een toestel aan te koppelen,
   en anderzĳds wanneer het lukt om een eboek over te hevelen.

2. **Onmiddelijk zichtbaar effect:** Wanneer een boek overgeheveld wordt, 
   verschijnt dat boek nu ook onmiddelijk in het overzichtspaneel van dat 
   apparaat. Eerst verscheen dat eboek pas na het herstarten van Boekerĳ.

3. **Kaftkiesbaarheid:** Het is mogelijk om een afbeelding uit het eboek in te
   stellen als Boekerĳ-kaft. Dit wordt (nog) niet opgeslagen in het eboek zelf.
   
4. **Zoekfunctionaliteit:** Een zoekbalk is nu onderdeel van Boekerĳ zodat een
   overzicht gefilterd kan worden op titel of auteur.

4. **Meer glade:** Boekerĳ is overgestapt van geprogrammeerde gtk-elementen, 
   naar in glade gedefinieerde elementen. Dat bevordert het programmeer- en
   vertaalgemak.

5. **Verwĳderbevestiging:** Een verwĳdering geschiedt nu pas nadat de gebruiker
   ook die intentie bevestigd heeft.

6. **Rest:** Vele bugfixes, functieverfijningen, en betere herimplementaties van
   eerdere dingen.
