# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-30 17:50+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../middelen/gtk/boekzicht.ui:169 ../middelen/gtk/boekzicht.ui:292
msgid "Verwijderen"
msgstr ""

#: ../middelen/gtk/boekzicht.ui:199
msgid "Overhevelen"
msgstr ""

#: ../middelen/gtk/boekzicht.ui:227
msgid "Kaft kiezen"
msgstr ""

#: ../middelen/gtk/boekzicht.ui:249
msgid "Details"
msgstr ""

#: ../middelen/gtk/boekzicht.ui:261
msgid ""
"U staat op het punt dit boek definitief van uw computer te verwijderen. Wilt "
"u dit echt?"
msgstr ""

#: ../middelen/gtk/boekzicht.ui:279
msgid "Annuleren"
msgstr ""

#: ../middelen/gtk/hoofdmenu.ui:7 ../middelen/gtk/menu.ui:7
#: ../middelen/gtk/voorkeurenvenstertje.ui:8
msgid "Voorkeuren"
msgstr ""

#: ../middelen/gtk/hoofdmenu.ui:13 ../middelen/gtk/menu.ui:13
msgid "Info"
msgstr ""

#: ../middelen/gtk/hoofdmenu.ui:17 ../middelen/gtk/menu.ui:17
msgid "Afsluiten"
msgstr ""

#: ../middelen/gtk/meldingen.ui:98
msgid "Overgeheveld naar "
msgstr ""

#: ../middelen/gtk/meldingen.ui:166
msgid "Overhevelend naar "
msgstr ""

#: ../middelen/gtk/meldingen.ui:179
msgid "Overhevelend naar"
msgstr ""

#: ../middelen/gtk/oervenster.ui:34
msgid "Zoek naar boek"
msgstr ""

#: ../middelen/gtk/oervenster.ui:146
msgid "Niets te melden"
msgstr ""

#: ../middelen/gtk/overvenstertje.ui:12
msgid "Een werk van Arthur Nieuwland (2017-2018)"
msgstr ""

#: ../middelen/gtk/overvenstertje.ui:13
msgid ""
"Boekerĳ is een eboekbeheerder voor een eenvoudigere en Gnomigere beleving "
"van je digitale boekenverzameling"
msgstr ""

#: ../middelen/gtk/voorkeurenvenstertje.ui:26
msgid "Sluiten"
msgstr ""

#: ../middelen/gtk/voorkeurenvenstertje.ui:53
msgid "Boekenopslag"
msgstr ""

#: ../middelen/gtk/voorkeurenvenstertje.ui:69
msgid "Lokale boekenopslag kiezen"
msgstr ""

#: ../middelen/eu.nimmerfort.Boekerij.desktop:8
msgid "Ebook-bibliotheek"
msgstr ""

#: ../middelen/eu.nimmerfort.Boekerij.desktop:11
msgid "Beheer je ebooken"
msgstr ""

#: ../middelen/boekzicht.ui
msgid "Verwijderen..."
msgstr ""
