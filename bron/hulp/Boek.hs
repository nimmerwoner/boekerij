module Boek where


-------------
--- Steunwaar
-------------
import           Codec.Epub.Data.Metadata
import           Data.Char                  (toLower)
import           Data.Hashable
import           Data.List                  (minimumBy)
import           Data.String.Utils          (strip)
import           Data.Text                  (pack, take)

import           Allerhande
import           Hertaling

------------------
--- Datastructuren
------------------
data Boek = Boek
  {  _bestand :: BestandspadT
  , _metadata :: Metadata
  ,     _kaft :: Soms Kaft
  }


class Boekachtig a where
    isWelgevormd    :: a -> IO Bool
    verkrĳgKaft     :: a -> IO (Soms Kaft)
    verkrĳgMetadata :: a -> IO Metadata
    ontrafel        :: a -> IO (Metadata, Soms Kaft)
    leesBoek        :: a -> IO Boek


--------
--- Code
--------
boekTitel :: Boek -> String
boekTitel = foldr ((++) . titleText) "" . metaTitles . _metadata

boekTitel' :: Boek -> Tekst
boekTitel' = pack . boekTitel


boekAuteur :: Boek -> String
boekAuteur = foldr ((++) . creatorText) "" . metaCreators . _metadata

boekAuteur' :: Boek -> Tekst
boekAuteur' = pack . boekAuteur


boekUitgave :: Boek -> String
boekUitgave boek = do
  let datums = metaDates $ _metadata boek
  if null datums
  then ""
  else do let (Date _ beste) = minimumBy besteDatum datums
          beste
 where besteDatum d1 d2 = vernummer d1 `compare` vernummer d2
       vernummer :: Date -> Int
       vernummer (Date Nothing _)       = 2
       vernummer (Date (Just inhoud) _)
         | strip inhoud == "" = 2
         | veronderkast (strip inhoud) == "publication" = 1
         | otherwise = 3


boekUitgave' :: Boek -> Tekst
boekUitgave' = pack . boekUitgave


boekJaar' :: Boek -> Tekst
boekJaar' = Data.Text.take 4 . boekUitgave'


boekVerplaatsnaam :: Boek -> String
boekVerplaatsnaam boek =
  zuiver (verbovenkast (boekAuteur boek) ++ ", " ++ boekTitel boek ++ ".epub")


zuiver :: String -> String
zuiver [] = []
zuiver (c:rest)
  | toLower c `elem` wettig
           =  c  : zuiver rest
  | anders = '_' : zuiver rest
 where wettig :: String
       wettig = "abcdefghijklmnopqrstuvwxyz0123456789.,-_[]{}«»<>;?|!@#$%^&*() "
zuiver (_:_) = "ONMOGELIJKE NAAM, Onmogelijke titel"


instance Eq Boek where
  boek1 == boek2 = do
      let titel1  = boekTitel  boek1
          titel2  = boekTitel  boek2
          auteur1 = boekAuteur boek1
          auteur2 = boekAuteur boek2
      titel1 == titel2 && auteur1 == auteur2


instance Ord Boek where
  boek1 `compare` boek2 = do
      let compA = veronderkast (boekAuteur boek1) `compare`
                  veronderkast (boekAuteur boek2)
          compT = veronderkast (boekTitel boek1) `compare`
                  veronderkast (boekTitel boek2)
          compD = boekUitgave boek1 `compare` boekUitgave boek2

      foldr (\v1 v2 -> if v1 /= EQ
                       then v1
                       else v2)
            EQ [compA, compD, compT]


instance Hashable Boek where
  hashWithSalt salt = hashWithSalt salt . _bestand
  hash = hashWithSalt 0
