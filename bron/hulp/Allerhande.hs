{-# LANGUAGE OverloadedStrings #-}

module Allerhande where

------------------
-- Eigen steunwaar
------------------
import           Hertaling


-----------------------
-- Steunwaar van derden
-----------------------
import           Control.Exception

import           Data.Char             (toLower, toUpper)
import           Data.List             (isSuffixOf)
import           Data.Text             (pack, unpack)

import           System.Directory      (doesFileExist)

--------
--- Code
--------
(<<<) :: Monad m => (a -> m b) -> m a -> m a
(<<<) fb a = do
  fb =<< a
  a


mits :: Bool -> IO a -> IO (Maybe a)
mits voorwaarde gevolg = if voorwaarde
                         then Just <$> gevolg
                         else niets


leegte :: IO ()
leegte = geef ()


niets :: IO (Maybe a)
niets = geef Nothing


putStrT :: Tekst -> IO ()
putStrT   = putStr . unpack


putStrLnT :: Tekst -> IO ()
putStrLnT = putStrLn . unpack


isBoekT :: BestandspadT -> IO Bool
isBoekT = isBoek . unpack


isBoek :: Bestandspad -> IO Bool
isBoek pad = do
  -- 1. Is het wel een bestand?
  bestand <- doesFileExist pad
  -- 2. Controleer extensie
  let epub = ".epub" `isSuffixOf` veronderkast pad
  geef $ bestand && epub


catchAny :: IO a -> (SomeException -> IO a) -> IO a
catchAny = catch


veronderkast :: String -> String
veronderkast = map toLower


verbovenkast :: String -> String
verbovenkast = map toUpper


vertekst :: Show a => a -> Tekst
vertekst = pack . show


-- Gokt of een gegeven string de bestandsnaam / het bestandspad van een
-- afbeelding is op basis van de extensie.
gisOfBeeld :: String -> Bool
gisOfBeeld s = foldr ((||) . flip isSuffixOf onderzochte) False exts
 where onderzochte = veronderkast s
       exts = [".jpg", ".jpeg", ".gif", ".png"]
