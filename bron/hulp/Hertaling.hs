module Hertaling
  ( module Hertaling
  , (<>)
  ) where

-----------------------
--- Hertaalde steunwaar
-----------------------
import           Data.Text           (Text)
import           GI.Gtk              (Image)
import           Data.Monoid         ((<>))


-------------------------
--- Hertalingen van types
-------------------------
type Kaft = Image

type Tekst = Text

type URL = Tekst

type Soms a = Maybe a

type Bestandspad  = FilePath
type BestandspadT = Tekst


----------------------------
--- Hertalingen van functies
----------------------------
geef :: Monad m => a -> m a
geef = return

anders :: Bool
anders = otherwise

niet :: Bool -> Bool
niet = not

ofwel :: (a -> c) -> (b -> c) -> Either a b -> c
ofwel = either

soms :: b -> (a -> b) -> Maybe a -> b
soms = maybe

onbepaald :: a
onbepaald = undefined
