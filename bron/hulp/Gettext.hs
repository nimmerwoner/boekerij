module Gettext 
  ( bindTekstdomein
  ) where


import Hertaling

import Control.Monad ((>=>))
import Data.Text (pack, unpack)

import           Data.GI.Base.BasicConversions (cstringToString)


import           Foreign.C.String (withCString, CString)
import           Foreign.Ptr      (nullPtr)


foreign import ccall unsafe "bindtextdomain" c_bindtextdomain
    :: CString -> CString -> IO CString
    

bindTekstdomein :: Tekst -> Maybe Tekst -> IO Tekst
bindTekstdomein domeinnaam mapnaam =
  pack <$> withCString (unpack domeinnaam) (\domein ->
      withCStringMaybe mapnaam $ c_bindtextdomain domein >=> cstringToString)


withCStringMaybe :: Maybe Tekst -> (CString -> IO a) -> IO a
withCStringMaybe Nothing actie = actie nullPtr
withCStringMaybe (Just tekst) actie = withCString (unpack tekst) actie
