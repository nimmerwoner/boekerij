{-# LANGUAGE StandaloneDeriving #-}
{-# OPTIONS_GHC -fno-warn-orphans -fno-warn-type-defaults #-}

module AllerhandeGtk where

-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           Begrippen
import           Hertaling
import           Instellingen


------------------------
--- Steunwaar van derden
------------------------
import           Control.Monad.IO.Class (MonadIO)

import           Data.IORef
import           Data.Hashable
import           Data.Maybe    (fromJust, isNothing, catMaybes)
import           Data.Monoid   ((<>))
import           Data.Text     hiding (map, concat)

import           GHC.Word      (Word32)
import           GHC.Int       (Int32)

import qualified GI.Gdk        as Gdk
import           GI.GdkPixbuf
import qualified GI.GLib       as GLib
import qualified GI.Gio        as Gio
import           GI.Gtk

import           System.Directory (doesFileExist)


--------
--- Code
--------
laadBoekerĳKaft :: Integer -> BestandspadT -> IO (Soms Kaft) -- NOOT: Bestandspad van het ebook, niet de kaftafb
laadBoekerĳKaft hoogte bestandspad = do
  map <- pack <$> kaftenfolder
  let naam = vertekst $ hash bestandspad
  catchAny (laadAfbVanSchĳf hoogte (map <> "/" <> naam))
           (\_ -> geef Nothing)


laadAfbVanSchĳf :: Integer -> BestandspadT -> IO (Soms Kaft)
laadAfbVanSchĳf hoogte pad = do
  let br = fromIntegral $ round $ fromIntegral hoogte / begrKaftratio :: Int32
      ho = fromIntegral hoogte :: Int32
  catchAny (do pb <- Just <$> pixbufNewFromFileAtScale pad br ho True
               Just <$> imageNewFromPixbuf pb)
           (\_ -> geef Nothing)


pixbufLaadVanSchĳf :: Integer -> BestandspadT -> IO (Soms Pixbuf)
pixbufLaadVanSchĳf hoogte pad = do
  let br = fromIntegral $ round $ fromIntegral hoogte / begrKaftratio
      ho = fromIntegral hoogte
  catchAny (Just <$> pixbufNewFromFileAtScale pad br ho True)
           (\_ -> geef Nothing)


laadStedehouderKaft :: Integer -> IO (Soms Kaft)
laadStedehouderKaft grootte = laadAfbVanSchĳf grootte begrStedehouder


rits :: MonadIO m => IO a -> m Word32
rits opdracht = Gdk.threadsAddIdle GLib.PRIORITY_DEFAULT
                                   (opdracht >> geef False)


containerVerwĳderKinderen :: IsContainer c => c -> IO ()
containerVerwĳderKinderen houder = do
  kindn <- containerGetChildren houder -- bevat kaft
  mapM_ (containerRemove houder) kindn


bouwerNieuw :: BestandspadT -> IO Builder
bouwerNieuw glade = do
  --(flip builderAddFromFile glade) <<< builderNew
  bestaat <- doesFileExist $ unpack glade
  if bestaat then leegte else foutbericht
  bwr <- builderNew
  builderAddFromFile bwr glade
  builderSetTranslationDomain bwr (Just "eu.nimmerfort.boekerĳ")
  geef bwr
 where foutbericht  = putStrLnT $ "Bedieningsbron bestaat niet: " <> glade
       --slaagbericht = "Bedieningsbron bestaat: " <> glade


laadGtkVeilig :: (GObject o) => Tekst -> (ManagedPtr o -> o) -> Builder
              -> IO (Maybe o)
laadGtkVeilig naam doel bouwer = do
  sObject <- builderGetObject bouwer naam
  maybe (laadfeiling >> niets)
        (\obj -> do sOmgevormd <- castTo doel obj
                    maybe (omvormfeiling >> niets)
                          (geef <$> Just)
                          sOmgevormd)
        sObject
 where laadfeiling = putStrLnT $ "Fout bĳ laden van " <> naam
       omvormfeiling = putStrLnT $ "Fout bĳ omvormen van " <> naam


laadGtk :: GObject o => Tekst -> (ManagedPtr o -> o) -> Builder -> IO o
laadGtk naam soort bouwer = do
  ding <- laadGtkVeilig naam soort bouwer
  if isNothing ding
  then putStrLnT $ "Onverwachte Nothing bĳ laden van " <> naam
  else geef ()
  geef $ fromJust ding


verkrĳgMounts :: Gio.VolumeMonitor -> IO [Gio.Mount]
verkrĳgMounts schouwer = do
  drives <- Gio.volumeMonitorGetConnectedDrives schouwer :: IO [Gio.Drive]
  vols   <- concat <$> mapM Gio.driveGetVolumes drives   :: IO [Gio.Volume]
  --mounts <- mapM Gio.volumeGetMount vols
  --geef mounts
  mounts <- mapM Gio.volumeGetMount vols                 :: IO [Maybe Gio.Mount]
  geef $ catMaybes mounts


mountVerkrĳgPad :: Gio.Mount -> IO Bestandspad
mountVerkrĳgPad mount = do
  oerfolder <- Gio.mountGetRoot mount
  fromJust <$> Gio.fileGetPath oerfolder


listStoreGrootte :: ListStore -> IO Int
listStoreGrootte leest = do
  boomLeest <- unsafeCastTo TreeModel leest
  teller <- newIORef 0
  treeModelForeach boomLeest (tel teller)
  readIORef teller
 where tel :: IORef Int -> TreeModel -> TreePath -> TreeIter -> IO Bool
       tel teller _ _ _ = do
         modifyIORef teller (1+)
         geef False


koppelActie :: Application -> Tekst -> (Maybe Gio.GVariant -> IO ()) -> IO ()
koppelActie toepassing naam opdracht = do
  handeling <- Gio.simpleActionNew naam Nothing
  Gio.actionMapAddAction toepassing handeling
  handeling `Gio.onSimpleActionActivate` opdracht
  leegte


deriving instance Eq (ManagedPtr a)
deriving instance Eq Widget
deriving instance Eq Box
deriving instance Eq EventBox
deriving instance Eq TreePath
deriving instance Eq TreeIter
