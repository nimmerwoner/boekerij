{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module Begrippen where

import           Hertaling

import           Data.Monoid              ((<>))

--------------------------------------------------------------------------------
--- Programmabegrippen ---------------------------------------------------------
--------------------------------------------------------------------------------
begrVersie    = "0.2.0.0"                                 :: Tekst
begrPrognaamT = "Boekerĳ"                                 :: Tekst
begrPrognaamS = "Boekerĳ"                                 :: String
begrPrognaamVeiligT = "Boekerij"                          :: Tekst
begrPrognaamVeiligS = "Boekerij"                          :: String
begrDnsNaamVeilig   = "eu.nimmerfort.Boekerij"            :: Tekst
begrDnsNaam         = "eu.nimmerfort.Boekerĳ"             :: Tekst

--------------------------------------------------------------------------------
--- Programmabestanden ---------------------------------------------------------
--------------------------------------------------------------------------------
begrTalen    = PREFIX_TALEN                     :: BestandspadT
begrMiddelen = PREFIX                           :: BestandspadT
begrSqldb    = begrMiddelen <> "db1.sqlite"     :: BestandspadT
begrGMenu    = begrMiddelen <> "gtk/menu.ui"    :: BestandspadT

--------------------------------------------------------------------------------
--- Standaardinstellingen ------------------------------------------------------
--------------------------------------------------------------------------------
begrStedehouder = begrMiddelen <> "afbeeldingen/paper-icons-kaftloos.png" :: BestandspadT
begrKaftratio   = 1.38                                       :: Double
begrKafthoogte  = 250                                        :: Integer

--------------------------------------------------------------------------------
--- Structuur van Toestellen-ListStore -----------------------------------------
--------------------------------------------------------------------------------
begrKolomToestelNaam   = 0                                   :: Integer
begrKolomToestelWortel = 1                                   :: Integer
