module Instellingen where


-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           Begrippen
import           Hertaling


------------------------
--- Steunwaar van derden
------------------------
import           Prelude                 hiding (lookup)

import qualified Data.Text               as T
import           Data.String.Utils       (strip)

import           System.Directory
import           System.FilePath


------------------------
--- Eigen datastructuren
------------------------
data Instellingen = Instellingen
  {  _hoofdberging :: Bestandspad -- De map met de boekenverzameling
  , _googlesleutel :: String
  } deriving Show


--------------------------------------------
--- Functies die standaardwaarden op leveren
--------------------------------------------
-- ~/.cache/boekerij
tussenopslag :: IO Bestandspad
tussenopslag =  getXdgDirectory XdgCache $ veronderkast begrPrognaamVeiligS


-- ~/.local/share/boekerij
gegevensopslag :: IO Bestandspad
gegevensopslag =  getXdgDirectory XdgData $ veronderkast begrPrognaamVeiligS


kaftenfolder :: IO Bestandspad
kaftenfolder = do
  boven <- tussenopslag
  geef $ boven </> "kaften"


terugvalopslag :: IO Bestandspad
terugvalopslag = do
  mapHome <- getHomeDirectory
  geef $ mapHome </> "Boeken"


terugvalGooglesleutel :: String
terugvalGooglesleutel = ""


terugvalinstellingen :: IO Instellingen
terugvalinstellingen = do
  opslag <- terugvalopslag
  geef $ Instellingen opslag terugvalGooglesleutel


---------------------------------------
--- Functies die met de settings werken
---------------------------------------
raadpleegInstellingen :: IO Instellingen
raadpleegInstellingen = do
  tb   <- terugvalopslag
  bpad <- raadpleeg instellingOpslag tb
  gsl  <- raadpleeg instellingGooglesleutel terugvalGooglesleutel
  geef $ Instellingen bpad gsl


raadpleeg :: String -> String -> IO String
raadpleeg instelling terugvalwaarde = do
  folder <- gegevensopslag
  catchAny (readFile $ folder </> instelling) (\_ -> geef terugvalwaarde)
 --where lees pad = do
 --        regels <- map strip . lines <$> readFile pad
 --        geef $ head regels -- eventuele fouten opgevangen door catchAny


legVast :: String -> String -> IO ()
legVast instelling waarde = do
  folder <- gegevensopslag
  catchAny (writeFile (strip $ folder </> instelling) waarde) (const leegte)


legVastT :: String -> Tekst -> IO ()
legVastT instelling waarde = legVast instelling (T.unpack waarde)


instellingOpslag :: String
instellingOpslag = "opslag"

instellingGooglesleutel :: String
instellingGooglesleutel = "google-sleutel"
