{-# LANGUAGE OverloadedStrings #-}

module InUit where

-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           Boek
import           Hertaling

------------------------
--- Steunwaar van derden
------------------------
import qualified Data.Text         as T
import           GI.Gio            (fileCopy, Cancellable, fileNewForPath, FileCopyFlags(..))
import           System.Directory  (removeFile, createDirectoryIfMissing)
import           System.FilePath


--------
--- Code
--------
verwĳderBoek :: Boek -> IO Bool
verwĳderBoek boek = do
  let bestand = T.unpack $ _bestand boek
  (removeFile bestand >> geef True) `catchAny` verwerkHinder
 where verwerkHinder hinder = print hinder >> geef False


hevelBoekOver :: Boek -> Bestandspad -> IO Bool
hevelBoekOver boek doel = do
  let bestand = T.unpack $ _bestand boek
  let doel' = doel </> boekVerplaatsnaam boek

  b_in  <- fileNewForPath bestand
  b_uit <- fileNewForPath doel'

  createDirectoryIfMissing False doel

  -- TODO Foutafhandeling
  fileCopy b_in b_uit [FileCopyFlagsOverwrite]
      (Nothing::Soms Cancellable) Nothing
  geef True
  --(copyFile bestand doel' >> geef True) `catchAny` verwerkHinder
 --where verwerkHinder hinder = geef False
