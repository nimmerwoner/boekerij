{-# LANGUAGE OverloadedStrings #-}

module VerwerkingEpubs where

-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           AllerhandeGtk
import           Begrippen
import           Boek
import           Hertaling
import           Instellingen


------------------------
--- Steunwaar van derden
------------------------
import           Codec.Epub
import           Codec.Epub.Data.Metadata

import           Control.Monad.Except

import qualified Data.ByteString            as B
import qualified Data.ByteString.Builder    as BB
import           Data.Text                  (isSuffixOf, pack, toLower, unpack)
import           Data.Hashable              (hash)

import           System.Exit                (ExitCode(..))
import           System.IO
import           System.Process
import           System.FilePath


------------------
--- Datastructuren
------------------
newtype Epub = Epub BestandspadT


--------
--- Code
--------
instance Boekachtig Epub where
  isWelgevormd (Epub pad) = do
    let suffix = ".epub" `isSuffixOf` toLower pad
    welgevormd <- if suffix
                  then withFile (unpack pad) ReadMode keur
                  else geef False

    mits (niet welgevormd) (putStrT "Onwelgevormd: " >> putStrLnT pad)
    geef welgevormd
   where keur h = do
           aanloop  <- BB.toLazyByteString . BB.byteStringHex <$> B.hGet h 4
           mimetype <- B.drop 26 <$> B.hGet h 54
           return $ aanloop  == "504b0304" && -- niet-lege zip magic number
                    mimetype == "mimetypeapplication/epub+zip" -- epub mimetype


  ontrafel epub = do
      meta <- verkrĳgMetadata epub
      mAfb <- verkrĳgKaft epub
      geef (meta, mAfb)


  verkrĳgKaft (Epub pad) = do
      vbpad <- pack <$> berekenVoorbeeldpad pad
      sKaft <- laadBoekerĳKaft begrKafthoogte pad      :: IO (Soms Kaft)
      maybe (voorbeeldmaker vbpad) (geef . Just) sKaft :: IO (Soms Kaft)
    where voorbeeldmaker vbpad = do
            let inpad  = unpack pad
            let uitpad = unpack vbpad
            geslaagd <- catchAny (maakVoorbeeld inpad uitpad)
                                 (\e -> print e >> geef False)
            if   niet geslaagd then niets
            else laadBoekerĳKaft begrKafthoogte pad :: IO (Soms Kaft)


  verkrĳgMetadata (Epub pad) = do
      gevondenMd <- runExceptT $ do
        xmlString <- getPkgXmlFromZip $ unpack pad
        metadata  <- getMetadata xmlString

        geef metadata
      either (\_ -> geef emptyMetadata) geef gevondenMd


  leesBoek epub@(Epub pad) = do
    (md, mAfb) <- ontrafel epub
    geef $ Boek pad md mAfb

--- Einde instance declaratie

berekenVoorbeeldpad :: BestandspadT -> IO Bestandspad
berekenVoorbeeldpad padBoek = do
  map <- kaftenfolder
  geef $ map </> show (hash padBoek)
  

-- TODO gnome-epub-thumbnailer transcriberen naar Haskell
maakVoorbeeld :: String -> String -> IO Bool
maakVoorbeeld inpad uitpad = do 
  -- ongeïnteresseerd in waarschn: niet elk boek heeft een kaft
  let prog  = "gnome-epub-thumbnailer"
  let prcs  = (proc prog [inpad, uitpad]){ std_err = NoStream }
  exit <- withCreateProcess prcs (\_ _ _ process -> waitForProcess process)
  case exit of
    ExitSuccess   -> geef True
    ExitFailure _ -> geef False
