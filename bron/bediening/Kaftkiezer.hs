{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE OverloadedStrings #-}

module Kaftkiezer
  ( nieuw
  , toon
  ) where


-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           AllerhandeGtk
import           Begrippen
import           Boek
import           Boekzicht hiding (nieuw)
import           Hertaling


------------------------
--- Steunwaar van derden
------------------------
import           Codec.Archive.Zip

import qualified Data.ByteString.Lazy       as BL

import           GI.Gtk

import           Data.Maybe          (catMaybes)
import           Data.Text           (pack, unpack)
import           System.Directory    (getTemporaryDirectory)
import           System.IO.Temp      (withTempFile)

type Kaftkiezer = (Window, FlowBox)


-------
-- Code
-------
nieuw :: IO (Soms Kaftkiezer)
nieuw = do
  -- Laad UI
  bouwer <- bouwerNieuw glade
  
  sZoekvenster <- venstertje bouwer
  sHouder      <- overzicht  bouwer

  -- Teruggave
  maybe niets (\zv -> 
          maybe niets (\hr -> geef $ Just (zv, hr)) sHouder) 
        sZoekvenster


toon :: ApplicationWindow -> Boekzicht -> IO ()
toon venster boektoestand = do
  sKiezer <- nieuw
  maybe leegte 
        (\(venstertje, overzicht) -> do 
            windowSetTransientFor venstertje (Just venster)
            widgetShowAll venstertje
            
            -- Toon afbeeldingen in eboek  
            beelden <- zoekInBoek boektoestand venstertje overzicht
            rits $ mapM_ (voegIn venstertje overzicht) beelden
            leegte)
        sKiezer
            
  leegte
 where voegIn kaftZkr houder kaft = do
         box <- bouwKafthouder kaftZkr kaft boektoestand
         containerAdd houder box
         widgetShowAll box


zoekInBoek :: Boekzicht -> Window -> FlowBox -> IO [Kaft]
zoekInBoek boektoestand _ _ = do
  sKaften <- catchAny (do let bestand = unpack $ _bestand $ _boek boektoestand
                          zip <- BL.readFile bestand
                          let eArchief = toArchiveOrFail zip
                          either meldMislukking onttrekBeelden eArchief)
                      (const niets)
  geef $ maybe [] catMaybes sKaften
 where meldMislukking msg = do -- Indien uitpakken mislukte
         putStrLnT $ _bestand $ _boek boektoestand
         putStrLn msg
         putStrLn ""
         niets
       -- TODO Ook als het uitpakken slaagde, kan het een versleuteld archief
       --      zijn met ontoegankelijk bestanden. findEntry en fromEntry kunnen
       --      daardoor mislukken en moeten eigenlijk omwikkeld zijn met een
       --      catch. Nu is dat op een hoger niveau gedaan (hierboven)
       onttrekBeelden archief = do -- Indien uitpakken slaagde
         let beelden = filter gisOfBeeld $ filesInArchive archief
         let onttrekBeeld gevondenBeeld = do -- :: String -> IO ()
               let sItem = findEntryByPath gevondenBeeld archief
               vrlMap <- getTemporaryDirectory
               afb <- withTempFile vrlMap "bkrĳ-kaft" $ \vrl handle -> 
                          maybe (geef Nothing)
                                (\item -> do BL.hPut handle $ fromEntry item
                                             let h = begrKafthoogte
                                             laadAfbVanSchĳf h $ pack vrl)
                                sItem
               geef afb
         Just <$> mapM onttrekBeeld beelden


--zoekKaften :: ToestandBoek -> Window -> Container -> IO [()]
--zoekKaften boektoestand kaftZkr container = do
--  kaften <- kaften'
--  vrlMap <- getTemporaryDirectory
--  catchAny -- TODO vernuftigere faalverwerking
--    (sequence $ take 9 $ flip map kaften $ \url ->
--       withTempFile vrlMap "kaft" $ \vrlBestand handle -> do
--         downloadAfbeelding url handle
--         mKaft <- laadAfbVanSchĳf begrKafthoogte (pack vrlBestand)
--         case mKaft of
--           Just kaft -> do rits $ voegGevondenKaftToe kaft
--                           leegte
--           Nothing   -> leegte)
--    (\_ -> geef [()])
--
-- where voegGevondenKaftToe kaft = do
--         box <- bouwKafthouder kaftZkr kaft boektoestand
--         containerAdd container box
--         widgetShowAll box :: IO ()
--       kaften' :: IO [URL]
--       kaften' = do
--         let boek = _boek boektoestand
--         let beschr = pack $ boekTitel boek ++ " " ++ boekAuteur boek
--         kaften1 <- zoekKaftenGCustomS beschr
--         --kaften2 <- zoekKaftenGBooks beschr
--         --geef $ vervlecht kaften1 kaften2
--         geef kaften1


bouwKafthouder :: Window -> Kaft -> Boekzicht -> IO EventBox
bouwKafthouder kaftZkr kaft boektoestand = do
  let opdracht _ = do
        rits $ _kaftvervanger boektoestand boektoestand kaft
        widgetDestroy kaftZkr
        return True
  maakKlikbaar kaft opdracht


--gebruikKaft :: Boek -> Kaft -> IO (Soms Tekst)
--gebruikKaft boek kaft = do
--  -- verplaatst Kaft naar map van programma
--  map <- kaftenfolder
--  let opslagpad  = map </> show (hash $ _bestand boek)
--  let opslagpadT = pack opslagpad
--  createDirectoryIfMissing True map
--  sPixbuf <- imageGetPixbuf kaft
--  maybe niets
--        (\pb -> do pixbufSavev pb opslagpadT "jpeg" [] []
--                   geef $ Just opslagpadT)
--        sPixbuf


maakKlikbaar :: Kaft -> WidgetButtonPressEventCallback -> IO EventBox
maakKlikbaar afb opdracht = do
  afbBox <- eventBoxNew
  onWidgetButtonPressEvent afbBox opdracht
  containerAdd afbBox afb
  geef afbBox


-- Glade -----------------------------------------------------------------------

glade :: BestandspadT
glade = begrMiddelen <> "gtk/kaftkiezer.ui"

venstertje :: Builder -> IO (Soms Window)
venstertje = laadGtkVeilig "kaftkiesvenstertje" Window

overzicht :: Builder -> IO (Soms FlowBox)
overzicht  = laadGtkVeilig "kaftenoverzicht" FlowBox
