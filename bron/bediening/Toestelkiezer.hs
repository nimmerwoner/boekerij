{-# LANGUAGE OverloadedStrings #-}

module Toestelkiezer
  ( Toestelkiezer
  , toestelkiezer
  , toestellenknop
  , overhevelknop
  , koppelLeest
  , verkrĳgKnop
  , verkrĳgKiezer
  , isGevoelig
  , verkrĳgGekozenMap
  ) where


-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           AllerhandeGtk
import           Begrippen
import           Hertaling


------------------------
--- Steunwaar van derden
------------------------
import qualified Data.Text as T
import           GI.Gtk


type Toestelkiezer = Box


-------
-- Code
-------
isGevoelig :: Toestelkiezer -> Bool -> IO ()
isGevoelig tk bool = do
  kiezer <- verkrĳgKiezer tk
  knop   <- verkrĳgKnop tk
  widgetSetSensitive kiezer bool
  widgetSetSensitive knop   bool
  leegte


-- NOOT gedrag is ongedefinieerd voor dit meermaals op dezelfde toestelkiezer
-- aanroepen
koppelLeest :: Toestelkiezer -> ListStore -> IO ()
koppelLeest tk leest = do
  kiezer <- verkrĳgKiezer tk
  onTreeModelRowDeleted  leest $ \_   -> herstelGevoeligheidEnKeuze tk leest
  onTreeModelRowInserted leest $ \_ _ -> herstelGevoeligheidEnKeuze tk leest

  comboBoxSetModel kiezer (Just leest)
  herstelGevoeligheidEnKeuze tk leest


verkrĳgKnop :: Toestelkiezer -> IO Button
verkrĳgKnop tk = do
  kindn <- containerGetChildren tk
  let knop = kindn !! 1    -- Knop heeft index 1 want als tweede toegevoegd
  unsafeCastTo Button knop -- TODO veiliger maken


verkrĳgKiezer :: Toestelkiezer -> IO ComboBoxText
verkrĳgKiezer tk = do
  kindn <- containerGetChildren tk
  let cmbx = head kindn         -- Cmbx heeft index 0 want als eerste toegevoegd
  unsafeCastTo ComboBoxText cmbx -- TODO veiliger maken


verkrĳgGekozenMap :: Toestelkiezer -> IO (Soms Bestandspad)
verkrĳgGekozenMap tk = do
  cmbx <- verkrĳgKiezer tk
  lst  <- comboBoxGetModel cmbx
  idx  <- T.pack . show <$> comboBoxGetActive cmbx
  pad  <- treePathNewFromString idx
  (_, iter) <- treeModelGetIter lst pad -- TODO verwerk dat benadering mislukt
  waarde <- treeModelGetValue lst iter (fromIntegral begrKolomToestelWortel)
  fromGValue waarde


herstelGevoeligheidEnKeuze :: Toestelkiezer -> ListStore -> IO ()
herstelGevoeligheidEnKeuze tk leest = do
  kiezer  <- verkrĳgKiezer tk
  idx     <- comboBoxGetActive kiezer
  grootte <- listStoreGrootte leest
  if idx < 0
  then comboBoxSetActive kiezer 0
  else leegte
  isGevoelig tk (grootte > 0)


-- Glade -----------------------------------------------------------------------

toestelkiezer :: Builder -> IO Box
toestelkiezer = laadGtk "menu_toestelkiezer" Box

toestellenknop :: Builder -> IO ComboBoxText
toestellenknop = laadGtk "menu_toestellen" ComboBoxText

overhevelknop :: Builder -> IO Button
overhevelknop = laadGtk "menu_overhevelen" Button 
