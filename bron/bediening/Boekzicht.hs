{-# LANGUAGE OverloadedStrings #-}

module Boekzicht
  ( Boekzicht(..)
  , Boekzicht.nieuw
  , vervangKaft
  , isTuigje
  ) where


-------------------
--- Eigen steunwaar
-------------------
import           AllerhandeGtk
import           Begrippen
import           Boek
import           Contextmenu
import           Hertaling


------------------------
--- Steunwaar van derden
------------------------
import           Data.Maybe               (fromMaybe)
import           GI.GdkPixbuf
import           GI.Gtk


data Boekzicht = Boekzicht
  {          _boek :: Boek
  ,        _houder :: Box
  , _kaftvervanger :: Boekzicht -> Kaft -> IO ()
  ,    _kafthouder :: EventBox
  ,   _contextmenu :: Contextmenu
  ,    _oervenster :: ApplicationWindow
  }


-------
-- Code
-------
nieuw :: ApplicationWindow -> Pixbuf -> Soms ListStore -> Boek -> IO Boekzicht
nieuw venster kaftpixbuf mLeest boek = do
  bouwer     <- bouwerNieuw glade
  boekhouder <- boekhouder bouwer
  kafthouder <- kafthouder bouwer
  titel      <- titellabel bouwer
  schrv      <- schrĳverlabel bouwer
  
  menu <- bouwMenu bouwer boek mLeest

  labelSetMarkup titel $ boekTitel' boek
  labelSetMarkup schrv $ boekAuteur' boek

  onkaft <- imageNewFromPixbuf (Just kaftpixbuf)
  let kaft = fromMaybe onkaft (_kaft boek)
  widgetUnparent kaft -- FIXME WAAR WORDT HEM EERDER EEN OUDER GEGEVEN?!
  containerAdd kafthouder kaft
  onWidgetButtonReleaseEvent kafthouder (toonContextmenu menu)
  
  widgetShowAll boekhouder
  geef $ Boekzicht boek boekhouder vervangKaft kafthouder menu venster
 where toonContextmenu menu _ = do 
         popoverPopup $ _popover menu
         geef True


isTuigje :: Boekzicht -> Box -> Bool
isTuigje toestand tuigje = _houder toestand == tuigje


vervangKaft :: Boekzicht -> Kaft -> IO ()
vervangKaft toestand afb = do
  let kafthouder = _kafthouder toestand
  containerVerwĳderKinderen kafthouder      -- verwĳder oude kaft
  containerAdd kafthouder afb               -- plaats en toon nieuwe
  widgetShowAll kafthouder
  geef ()


instance Ord Boekzicht where
  bt1 `compare` bt2 = _boek bt1 `compare` _boek bt2


instance Eq Boekzicht where
  b1 == b2 = vergelĳk _boek && vergelĳk _houder && vergelĳk _kafthouder
   where vergelĳk :: Eq a => (Boekzicht -> a) -> Bool
         vergelĳk f = f b1 == f b2


-- Glade -----------------------------------------------------------------------

glade :: BestandspadT
glade = begrMiddelen <> "gtk/boekzicht.ui"
  
boekhouder :: Builder -> IO Box
boekhouder = laadGtk "boekhouder" Box 

kafthouder :: Builder -> IO EventBox
kafthouder = laadGtk "houder_kaft" EventBox

titellabel :: Builder -> IO Label
titellabel = laadGtk "houder_titel" Label 

schrĳverlabel :: Builder -> IO Label
schrĳverlabel = laadGtk "houder_schrijver" Label 
