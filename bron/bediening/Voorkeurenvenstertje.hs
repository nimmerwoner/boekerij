{-# LANGUAGE OverloadedStrings #-}

module Voorkeurenvenstertje
  ( trachtTeTonen
  ) where


-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           AllerhandeGtk
import           Begrippen
import           Hertaling
import           Instellingen


------------------------
--- Steunwaar van derden
------------------------
import           GI.Gtk


------------------------
--- Eigen datastructuren
------------------------
import OervensterDS


-------
-- Code
-------
trachtTeTonen :: Oervenster -> IO ()
trachtTeTonen toestand = do
  bwr <- bouwerNieuw glade
  sVoorkeuren <- voorkeurenvenstertje bwr
  case sVoorkeuren of
    Nothing -> leegte
    Just o -> do 
      sInvoer <- voorkeurOpslagpad bwr
      sSluit  <- sluitknop bwr
      
      windowSetTransientFor o (Just (_oervenster toestand))
      maybe leegte (\inv -> verbindBoekenopslag toestand inv o) sInvoer
      maybe leegte (`verbindSluiten` o) sSluit
      
      widgetShowAll o


verbindBoekenopslag :: Oervenster -> FileChooserButton -> Dialog -> IO ()
verbindBoekenopslag _ kiezer venstertje = do
  widgetSetSensitive kiezer True
  padBoekenopslag <- _hoofdberging <$> raadpleegInstellingen
  fileChooserSetFilename kiezer padBoekenopslag
  fileChooserSetCurrentFolder kiezer padBoekenopslag
  onWidgetDestroy venstertje verwerk
  leegte
 where verwerk = do 
         pad <- fileChooserGetFilename kiezer
         maybe leegte (legVast instellingOpslag) pad


verbindSluiten :: Button -> Dialog -> IO ()
verbindSluiten knop venstertje = onButtonReleased knop sluit >> leegte
 where sluit = widgetHide venstertje >> widgetDestroy venstertje


-- Glade -----------------------------------------------------------------------

glade :: BestandspadT
glade = begrMiddelen <> "gtk/voorkeurenvenstertje.ui"

-- Indien het voorkeurenvenstertje niet geladen kan worden is dat geen reden om
-- het programma te laten crashen. Laad deze daarom veilig
voorkeurenvenstertje :: Builder -> IO (Soms Dialog)
voorkeurenvenstertje = laadGtkVeilig "voorkeurenvenstertje" Dialog

voorkeurOpslagpad :: Builder -> IO (Soms FileChooserButton)
voorkeurOpslagpad = laadGtkVeilig "boekenopslagpad" FileChooserButton

sluitknop :: Builder -> IO (Soms Button)
sluitknop = laadGtkVeilig "sluitknop" Button
