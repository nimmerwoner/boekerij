{-# LANGUAGE OverloadedStrings #-}

module OervensterDS where


-------------
--- Steunwaar
-------------
import           Boek
import           Hertaling
import           Instellingen
import           GI.Gtk
import           GI.Gio (VolumeMonitor)

------------------------
--- Eigen datastructuren
------------------------
data Oervenster = Oervenster  -- Toestand van het gehele Boekerĳ-programma
  {           _bouwer :: Builder
  ,       _oervenster :: ApplicationWindow
  , _toestellenstapel :: Stack
  ,       _toestellen :: ListStore
  ,       _zeefinvoer :: SearchEntry
  ,         _zeefbalk :: SearchBar
  ,         _zeefknop :: ToggleButton
  ,    _meldingenknop :: ToggleButton
  ,  _meldingenpaneel :: Popover
  ,  _meldingenhouder :: Box
  ,  _toestelschouwer :: VolumeMonitor
  ,     _instellingen :: Instellingen
  }

data Melders = Melders
  {   _meldAangekoppelde :: Tekst -> IO ()
  , _meldOnaangekoppelde :: Tekst -> IO ()
  ,   _meldOvergehevelde :: Boek -> BestandspadT -> IO ()
  , _meldOnovergehevelde :: Boek -> BestandspadT -> IO ()
  ,      _meldVerwĳderde :: Boek -> IO ()
  ,    _meldOnverwĳderde :: Boek -> IO ()
  }
