{-# LANGUAGE OverloadedStrings #-}

module Oervenster
  ( module OervensterDS
  , Oervenster.nieuw
  , plaatsBoekenoverzicht
  , toonVoorkeuren
  , toonOver
  , meldOnaangekoppeldToestel
  , meldGeslaagdeOverheveling
  ) where


-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           AllerhandeGtk
import           Begrippen
import           Boek
import           Boekenoverzicht
import           Hertaling
import           Instellingen
import           Overvenstertje
import           Voorkeurenvenstertje


------------------------
--- Steunwaar van derden
------------------------
import           Control.Monad.IO.Class (MonadIO)
import qualified Data.Text    as T
import           GI.Gtk
import           GI.Gio          (volumeMonitorGet)
import           System.FilePath (takeDirectory, takeBaseName)


------------------------
--- Eigen datastructuren
------------------------
import OervensterDS


-------
-- Code
-------
nieuw :: IO Oervenster
nieuw = do
  bwr <- bouwerNieuw glade
  v   <- venster bwr
  bon <- toestellenstapel bwr
  tn  <- toestellen bwr
  zi  <- zeefinvoer bwr
  zb  <- zeefbalk bwr
  zk  <- zeefknop bwr
  mk  <- meldingenknop bwr
  mp  <- meldingenpaneel bwr
  mh  <- meldingenhouder bwr

  schwr <- volumeMonitorGet -- schouwen naar toestellen
  instn <- raadpleegInstellingen

  let toestand = Oervenster bwr v bon tn zi zb zk mk mp mh schwr instn
  verbindZoekbalk toestand
  verbindMeldingenpaneel toestand

  geef toestand


toonOver :: Oervenster -> IO ()
toonOver = Overvenstertje.trachtTeTonen

toonVoorkeuren :: Oervenster -> IO ()
toonVoorkeuren = Voorkeurenvenstertje.trachtTeTonen

plaatsBoekenoverzicht :: MonadIO m => Oervenster -> Boekenoverzicht -> Tekst 
                      -> m ()
plaatsBoekenoverzicht toestand overzicht naam = do
  let stapel = _toestellenstapel toestand
  let toner  = _schuiver overzicht
  stackAddTitled stapel toner naam naam


verbindMeldingenpaneel :: Oervenster -> IO ()
verbindMeldingenpaneel toestand = do
  let paneel = _meldingenpaneel toestand
  let knop   = _meldingenknop toestand
    
  onPopoverClosed paneel (toggleButtonSetActive knop False >> leegte)
  onToggleButtonToggled knop wisselZichtbaarheid

  widgetShowAll paneel
  leegte
 where wisselZichtbaarheid = do
         ingedrukt <- getToggleButtonActive (_meldingenknop toestand)
         if ingedrukt
         then toonMeldingenpaneel (_meldingenknop toestand)
                                  (_meldingenpaneel toestand)
         else popoverPopdown $ _meldingenpaneel toestand
         leegte


verbindZoekbalk :: Oervenster -> IO ()
verbindZoekbalk toestand = do
  searchBarConnectEntry (_zeefbalk toestand) (_zeefinvoer toestand)
  onToggleButtonToggled (_zeefknop toestand) wisselZichtbaarheid

  sneltoetsen  <- accelGroupNew
  (toets, mod) <- acceleratorParse "<Control>f"
  windowAddAccelGroup (_oervenster toestand) sneltoetsen
  widgetAddAccelerator (_zeefknop toestand) "activate" sneltoetsen toets mod []

  leegte
 where wisselZichtbaarheid = do
         ingedrukt <- getToggleButtonActive (_zeefknop toestand)
         searchBarSetSearchMode (_zeefbalk toestand) ingedrukt
--  onWidgetKeyPressEvent (_oervenster toestand) (afhandelaar balk)
--  leegte
-- where afhandelaar :: SearchBar -> Gdk.EventKey -> IO Bool
--       afhandelaar balk eventKey = do
--         event <- Gdk.unsafeCastTo Gdk.Event eventKey
--         searchBarHandleEvent balk eventKey


-- Meldingenpaneel -------------------------------------------------------------

toonMeldingenpaneel :: (IsPopover a1,IsWidget a2,MonadIO m) => a2 -> a1 -> m ()
toonMeldingenpaneel knop paneel = do
  stĳl <- widgetGetStyleContext knop
  styleContextRemoveClass stĳl "suggested-action"
  popoverPopup paneel


meldOnaangekoppeldToestel :: Oervenster -> Tekst -> IO ()
meldOnaangekoppeldToestel toestand naam = do
  bouwer   <- bouwerNieuw gladeMeldingen
  melding  <- meldingOnaankoppelbaar bouwer
  toestel  <- meldingOnaankoppelbaarToestel bouwer
  
  labelSetText toestel naam
  meld toestand melding


meldGeslaagdeOverheveling :: Oervenster -> Boek -> BestandspadT -> IO ()
meldGeslaagdeOverheveling toestand boek doel = do
  bouwer   <- bouwerNieuw gladeMeldingen
  melding  <- meldingOvergeheveld bouwer
  schrĳver <- meldingOvergeheveldSchrĳver bouwer
  titel    <- meldingOvergeheveldTitel    bouwer
  toestel  <- meldingOvergeheveldToestel  bouwer

  labelSetText schrĳver $ boekAuteur' boek
  labelSetText titel    $ boekTitel'  boek
  labelSetText toestel  $ T.pack $ takeBaseName $ takeDirectory $ T.unpack doel
  meld toestand melding


--meldMislukteOverheveling :: IO ()
--meldMislukteOverheveling = undefined


verbergNietsTeMelden :: Oervenster -> IO ()
verbergNietsTeMelden toestand = do
  nietstemelden' <- nietstemelden $ _bouwer toestand
  widgetHide nietstemelden'


meld :: Oervenster -> Box -> IO ()
meld toestand melding = do
  let houder = _meldingenhouder toestand
  boxPackEnd houder melding True False 0
  verbergNietsTeMelden toestand
  widgetShowAll melding
  meldMelding toestand


meldMelding :: Oervenster -> IO ()
meldMelding toestand = do 
  let knop = _meldingenknop toestand
  stĳl <- widgetGetStyleContext knop
  styleContextAddClass stĳl "suggested-action"
  leegte


-- Glade -----------------------------------------------------------------------

glade :: BestandspadT
glade = begrMiddelen <> "gtk/oervenster.ui"

gladeMeldingen :: BestandspadT
gladeMeldingen = begrMiddelen <> "gtk/meldingen.ui"

-- Wanneer een dezer onderdelen niet geladen kan worden is de fundamentele
-- werking van het programma ondergraven. Boekerĳ mag dan crashen en er is
-- daarom geen reden dit veilig te doen
venster :: Builder -> IO ApplicationWindow
venster = laadGtk "oervenster" ApplicationWindow

toestellenstapel :: Builder -> IO Stack
toestellenstapel = laadGtk "verzamelingenstapel" Stack

toestellen :: Builder -> IO ListStore
toestellen = laadGtk "leest_toestellen" ListStore

zeefinvoer :: Builder -> IO SearchEntry
zeefinvoer = laadGtk "zeefinvoer" SearchEntry

zeefbalk :: Builder -> IO SearchBar
zeefbalk = laadGtk "zeefbalk" SearchBar

zeefknop :: Builder -> IO ToggleButton
zeefknop = laadGtk "zeefknop" ToggleButton

meldingenknop :: Builder -> IO ToggleButton
meldingenknop = laadGtk "meldingenknop" ToggleButton

meldingenpaneel :: Builder -> IO Popover
meldingenpaneel = laadGtk "meldingenpaneel" Popover

meldingenhouder :: Builder -> IO Box
meldingenhouder = laadGtk "meldingenhouder" Box

nietstemelden :: Builder -> IO Label
nietstemelden = laadGtk "nietstemelden" Label

meldingOvergeheveld :: Builder -> IO Box
meldingOvergeheveld = laadGtk "overgeheveld" Box

meldingOvergeheveldSchrĳver :: Builder -> IO Label
meldingOvergeheveldSchrĳver = laadGtk "overgeheveld_schrĳver" Label

meldingOvergeheveldTitel :: Builder -> IO Label
meldingOvergeheveldTitel = laadGtk "overgeheveld_titel" Label

meldingOvergeheveldToestel :: Builder -> IO Label
meldingOvergeheveldToestel = laadGtk "overgeheveld_toestel" Label

meldingOnaankoppelbaar :: Builder -> IO Box
meldingOnaankoppelbaar = laadGtk "onaankoppelbaar" Box

meldingOnaankoppelbaarToestel :: Builder -> IO Label
meldingOnaankoppelbaarToestel = laadGtk "onaankoppelbaar_toestel" Label
