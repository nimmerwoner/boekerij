{-# LANGUAGE OverloadedStrings #-}

module Koppelaar
  ( Koppelaar.nieuw
  , Koppelaar(..)
  , koppelLeest
  , voegToestelToe
  , verwĳderToestel
  ) where


-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           AllerhandeGtk
import           Begrippen
import           Boekenoverzicht
import           Hertaling
import           Oervenster


------------------------
--- Steunwaar van derden
------------------------
import           Data.Maybe             (fromMaybe)
import           Data.Text.Read         (decimal)

import           System.Directory

import           GI.Gio                 (Mount, mountGetName)
import           GI.Gtk


------------------------
--- Eigen datastructuren
------------------------
data Koppelaar = Koppelaar
  {            _tabs :: Stack
  ,           _leest :: ListStore
  , _overzichtbouwer :: Bestandspad -> IO Boekenoverzicht -- Boekenoverzicht
  }


-------
-- Code
-------
nieuw :: Stack -> ListStore -> (Bestandspad -> IO Boekenoverzicht)
      -> IO Koppelaar
nieuw tabs leest schepper = do
  let toestand = Koppelaar tabs leest schepper
  koppelLeest toestand leest
  geef toestand


koppelLeest :: Koppelaar -> ListStore -> IO ()
koppelLeest toestand leest = do
  onTreeModelRowInserted leest $ toestelAangekoppeld toestand
  onTreeModelRowDeleted  leest $ toestelOntkoppeld   toestand
  leegte


-- Oervenster omwikkelt _toestellen::ListStore opzettelĳk, zie verbeidToestellen
voegToestelToe :: Oervenster -> Mount -> IO Bool
voegToestelToe toestand mnt = do
  let toestellen = _toestellen toestand
  catchAny -- permissies opvragen kan falen, vang dat af
    (do wortel <- mountVerkrĳgPad mnt
        permissies <- getPermissions wortel
        if writable permissies
        then do naam <- mountGetName mnt :: IO Tekst -- voeg aan leest toe
                gn   <- toGValue (Just naam)
                gw   <- toGValue (Just wortel)
                listStoreInsertWithValuesv toestellen (-1) [0, 1] [gn, gw]
                geef True
        else geef False)
    (const $ geef False)


-- Toestand omwikkelt _toestellen::ListStore opzettelĳk, zie verbeidToestellen
verwĳderToestel :: Oervenster -> Mount -> IO ()
verwĳderToestel toestand mnt = do
  naam <- mountGetName mnt
  let leest = _toestellen toestand
  listStoreVoorItem (_toestellen toestand) (Just naam) -- NOOIT Nothing meegeven
                    (\_ iter -> listStoreRemove leest iter >> leegte)


listStoreVoorItem :: (IsTreeModel a, Eq b, IsGValue b, Show b) => a -> b
                  -> (TreePath -> TreeIter -> IO ())
                  -> IO ()
listStoreVoorItem leest item opdracht = do
  boomLeest <- unsafeCastTo TreeModel leest
  treeModelForeach boomLeest (vind item)
 where vind :: (Eq a, IsGValue a, Show a) => a
            -> TreeModel -> TreePath -> TreeIter -> IO Bool
       vind item leest pad iter = do
         val <- fromGValue =<< treeModelGetValue leest iter 0
         if val == item
         then do opdracht pad iter
                 geef True -- beëindig de overloping van de leest
         else geef False


toestelAangekoppeld :: Koppelaar -> TreePath -> TreeIter -> IO ()
toestelAangekoppeld toestand _ iter = do
  sNaam   <- verkrĳg $ fromIntegral begrKolomToestelNaam
  sWortel <- verkrĳg $ fromIntegral begrKolomToestelWortel
  let nm = fromMaybe "Naamloos" sNaam

  maybe leegte
        (\wl -> do overzicht <- _overzichtbouwer toestand wl
                   stackAddTitled (_tabs toestand) (_schuiver overzicht) nm nm
                   widgetShowAll $ _tabs toestand)
        sWortel
  leegte
 where verkrĳg idx = fromGValue =<< treeModelGetValue (_leest toestand) iter idx


toestelOntkoppeld :: Koppelaar -> TreePath -> IO ()
toestelOntkoppeld toestand pad = do
  padStr <- treePathToString pad
  let Right (idx, _) = decimal padStr -- NOOT Crasht als niet ListStore!!
  let pos = idx + 1 -- +1 want in de stapel zit ook op pos 0 de lokale computer
  kindn <- containerGetChildren (_tabs toestand)
  containerRemove (_tabs toestand) (kindn !! pos)
