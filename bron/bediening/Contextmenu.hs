{-# LANGUAGE OverloadedStrings #-}

module Contextmenu
  ( Contextmenu(..)
  , boekmenu
  , bouwMenu
  , verkrĳgDoelmap
  ) where

-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           AllerhandeGtk
import           Begrippen
import           Boek
import           Hertaling
import           Toestelkiezer


------------------------
--- Steunwaar van derden
------------------------
import           GI.Gtk
import           System.FilePath ((</>))


------------------------
--- Eigen datastructuren
------------------------
data Contextmenu = Contextmenu
  {         _popover :: Popover
  ,  _knopVerwĳderen :: Button
  ,  _knopkaftKiezen :: Button
  , _knopOverhevelen :: Button
  ,   _toestelkiezer :: Soms Toestelkiezer
  }


--------
--- Code
--------
bouwMenu :: Builder -> Boek -> Maybe ListStore -> IO Contextmenu
bouwMenu bouwer boek mLeest = do
  menu <- boekmenu bouwer
  overhevelknop  <- overhevelknop bouwer
  verwĳderknop   <- verwĳderknop bouwer
  kaftkiesknop   <- kaftkiesknop bouwer
  annuleerknop   <- annuleerknop bouwer
  bevestigknop   <- bevestigknop bouwer
  panelen        <- panelen bouwer
  toestelkiezer  <- toestelkiezer bouwer
  titell         <- titellabel bouwer
  schrĳverl      <- schrĳverlabel bouwer
  bestandl       <- bestandlabel bouwer

  labelSetText titell    $ boekTitel' boek
  labelSetText schrĳverl $ boekAuteur' boek
  labelSetText bestandl  $ _bestand boek


  let links  = StackTransitionTypeSlideLeft
  let rechts = StackTransitionTypeSlideRight

  onButtonReleased overhevelknop (verberg menu)
  onButtonReleased kaftkiesknop  (verberg menu)
  onButtonReleased bevestigknop  (verberg menu)
  onButtonReleased verwĳderknop  (toon panelen "bevestigingspaneel" links)
  onButtonReleased annuleerknop  (toon panelen "detailpaneel" rechts)

  onPopoverClosed menu (toon panelen "detailpaneel" rechts)

  case mLeest of
    Nothing -> geef ()
    Just lt -> koppelLeest toestelkiezer lt

  geef $ Contextmenu menu bevestigknop kaftkiesknop
                     overhevelknop (Just toestelkiezer)
 where verberg menu = popoverPopdown menu >> leegte
       toon panelen doel richting = do
         stackSetTransitionType panelen richting
         stackSetVisibleChildName panelen doel


-- Als mToestelkiezer /= Nothing, dan is het contextmenu één niet gekoppeld aan
-- een boek op een toestel, en wél aan een boek op de computer.
-- Wanneer een boek van de computer is, moet de doelmap die van het gekozen
-- toestel zijn (vandaar dat mToestelkiezer /= Nothing).
-- Is het mTk wél Nothing, dan is het een boek van op een toestel dat naar de
-- computer overgeheveld moet worden. Dan moet de doelmap terugval zijn
verkrĳgDoelmap :: Contextmenu -> Bestandspad -> IO (Soms Bestandspad)
verkrĳgDoelmap toestand terugval = do
  let mToestelkiezer = _toestelkiezer toestand
  maybe (geef $ Just terugval) verkrĳg mToestelkiezer
 where verkrĳg tk = maybe Nothing verleng <$> verkrĳgGekozenMap tk
       verleng = Just . (</> begrPrognaamVeiligS)


-- Glade -----------------------------------------------------------------------

boekmenu :: Builder -> IO Popover
boekmenu = laadGtk "boekmenu" Popover

panelen :: Builder -> IO Stack
panelen = laadGtk "panelen" Stack

--detailpaneel :: Builder -> IO Box
--detailpaneel = laadGtk "detailpaneel" Box

titellabel :: Builder -> IO Label
titellabel = laadGtk "menu_titel" Label

schrĳverlabel :: Builder -> IO Label
schrĳverlabel = laadGtk "menu_schrijver" Label

jaarlabel :: Builder -> IO Label
jaarlabel = laadGtk "menu_jaar" Label

bestandlabel :: Builder -> IO Label
bestandlabel = laadGtk "menu_bestand" Label

kaftkiesknop :: Builder -> IO Button
kaftkiesknop = laadGtk "menu_kaft_kiezen" Button

verwĳderknop :: Builder -> IO Button
verwĳderknop = laadGtk "menu_verwijderen" Button

annuleerknop :: Builder -> IO Button
annuleerknop = laadGtk "menu_annuleren" Button

bevestigknop :: Builder -> IO Button
bevestigknop = laadGtk "menu_bevestigen" Button 
