{-# LANGUAGE OverloadedStrings #-}

module Boekenoverzicht
  ( Boekenoverzicht.nieuw
  , Boekenoverzicht(..)
  , lees
  , verbindZeef
  ) where


-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           AllerhandeGtk
import           Begrippen
import           Boek
import           Boekzicht
import           Hertaling
import           InUit
import           Kaftkiezer
import           VerwerkingEpubs


------------------------
--- Steunwaar van derden
------------------------
import           Control.Concurrent
import           Control.Monad

--import           Data.Hashable
--import qualified Data.HashMap        as Map
import           Data.Maybe          (fromJust, catMaybes)
import           Data.IORef
import           Data.List           (delete, sort, insert, elemIndex,
                                      partition, nub, isPrefixOf)
import qualified Data.Text           as T

import           Foreign.StablePtr   (newStablePtr)

import           System.Directory
import           System.FilePath
import           System.IO.Error

import           GI.GdkPixbuf
import           GI.Gio
import           GI.Gtk


------------------------
--- Eigen datastructuren
------------------------
import Contextmenu
import OervensterDS

data Boekenoverzicht = Boekenoverzicht
  {          _boeken :: IORef [Boekzicht]
  ,        _schuiver :: ScrolledWindow
  ,       _overzicht :: FlowBox
  ,         _berging :: Bestandspad -- de beschouwde folder
  , _boekzichtbouwer :: Boek -> IO Boekzicht
  }


--------
--- Code
--------
nieuw :: Bestandspad -> Melders -> (Boek -> IO Boekzicht)
      -> IO Boekenoverzicht
nieuw berging melders boekschepper = do
  verzameling <- newIORef [] :: IO (IORef [Boekzicht])

  bouwer    <- bouwerNieuw glade
  overzicht <- boekenoverzicht bouwer
  schuiver' <- schuiver bouwer

  let schepper' = schepper verzameling overzicht
  let toestand  = Boekenoverzicht verzameling schuiver' overzicht
                                  berging schepper'
  geef toestand
 where schepper :: IORef [Boekzicht] -> FlowBox -> Boek -> IO Boekzicht
       schepper verz overzicht boek = do
         boekzicht <- boekschepper boek
         integreerBoektuigje verz overzicht berging melders boekzicht
         geef boekzicht


--- Organisatie ----------------------------------------------------------------

voegIn :: Boekzicht -> IORef [Boekzicht] -> FlowBox -> IO ()
voegIn boektoestand verzameling overzicht = do
  idx <- voegInVerzameling boektoestand verzameling
  voegInOverzicht (_houder boektoestand) overzicht idx


voegInVerzameling :: Boekzicht -> IORef [Boekzicht] -> IO Int
voegInVerzameling boektoestand verzameling = do
  atomicModifyIORef' verzameling (\bn -> (insert boektoestand bn, ()))
  boeken <- readIORef verzameling
  geef $ fromJust $ elemIndex boektoestand boeken -- zou niet mogen falen


voegInOverzicht :: Box -> FlowBox -> Int -> IO ()
voegInOverzicht houder overzicht idx =
  flowBoxInsert overzicht houder (fromIntegral idx)


verwĳder :: Boekzicht -> IORef [Boekzicht] -> FlowBox -> IO ()
verwĳder boektoestand verzameling overzicht = do
  verwĳderUitOverzicht boektoestand overzicht
  verwĳderUitVerzameling boektoestand verzameling


verwĳderUitVerzameling :: Boekzicht -> IORef [Boekzicht] -> IO ()
verwĳderUitVerzameling boektoestand verzameling =
  atomicModifyIORef' verzameling (\bn -> (delete boektoestand bn, ()))


verwĳderUitOverzicht :: Boekzicht -> FlowBox -> IO ()
verwĳderUitOverzicht boektoestand _ = do
  sFlowBoxOuder <- widgetGetParent (_houder boektoestand)
  maybe leegte widgetDestroy sFlowBoxOuder
  widgetDestroy (_houder boektoestand)


zeef :: Tekst -> IORef [Boekzicht] -> IO ()
zeef naald verzameling = do
  boeken <- readIORef verzameling
  let (teTonen, teVerbergen) = partition (zoek naald) boeken
  mapM_ (toon False) teVerbergen
  mapM_ (toon True)  teTonen
 where toon bool boektoestand = do
         sOuder <- widgetGetParent (_houder boektoestand)
         maybe leegte (\ouder -> do -- Ouder is hoogstws. (99%) een FlowBoxChild
               sFbc <- castTo FlowBoxChild ouder
               maybe leegte (`widgetSetVisible` bool) sFbc
           ) sOuder


zoek :: Tekst -> Boekzicht -> Bool
zoek naald boektoestand =
  let boek = _boek boektoestand
      naald' = T.toLower $ T.strip naald
  in  naald' `T.isInfixOf` T.toLower (boekUitgave' boek) ||
      naald' `T.isInfixOf` T.toLower (boekAuteur'  boek) ||
      naald' `T.isInfixOf` T.toLower (boekTitel'   boek)


-- Bestanden -------------------------------------------------------------------
lees :: Boekenoverzicht -> IO ()
lees toestand = do
  forkIO $ do let overzicht = _overzicht toestand
              widgetSetSensitive overzicht False
              boeken <- vindBoeken (_berging toestand) verwerk
              let boekmappen = vindBoekmappen boeken
              mapM_ (schouw toestand) boekmappen
              widgetSetSensitive overzicht True
              leegte
  leegte
 where verwerk boek = do
         boektoestand <- _boekzichtbouwer toestand boek
         voegIn boektoestand (_boeken toestand) (_overzicht toestand)


vindBoeken :: Bestandspad -> (Boek -> IO ()) -> IO [Boek]
vindBoeken pad opdracht = do
  leespoging   <- tryIOError $ listDirectory pad
  alles        <- either (\_ -> geef []) geef leespoging
  let alles'   =  map (pad </>) alles
  bestanden    <- filterM isBoek alles'
  mappen       <- filterM doesDirectoryExist alles'
  boeken'      <- mapM (`metBoek` opdracht) bestanden
  let boeken    = catMaybes boeken'
  subboeken    <- concat <$> mapM (`vindBoeken` opdracht) mappen :: IO [Boek]
  geef $ sort $ boeken ++ subboeken


-- Van een lĳst met volledige paden naar eboeken maakt vindBoekmappen een lijst
-- met slechts de mappen waar die boeken zich in bevinden
vindBoekmappen :: [Boek] -> [Bestandspad]
vindBoekmappen = nub . map (takeDirectory . T.unpack . _bestand)


metBoek :: Bestandspad -> (Boek -> IO ()) -> IO (Maybe Boek)
metBoek pad opdracht = do
  let epub = Epub $ T.pack pad
  welgevormd <- isWelgevormd epub
  if niet welgevormd then niets
  else do boek <- leesBoek epub
          rits $ opdracht boek
          geef $ Just boek


-- Mapschouwing- ---------------------------------------------------------------

schouw :: Boekenoverzicht -> Bestandspad -> IO FileMonitor
schouw boekenoverzicht berging = do
  berging'  <- fileNewForPath berging
  schouwer  <- fileMonitorDirectory berging' [FileMonitorFlagsNone]
                   (Nothing::Soms Cancellable)
  _ <- newStablePtr schouwer

  onFileMonitorChanged schouwer $ verwerkWĳziging boekenoverzicht
  geef schouwer


verwerkWĳziging :: Boekenoverzicht -> File -> Soms File -> FileMonitorEvent
                -> IO ()
verwerkWĳziging _ bestand _ FileMonitorEventDeleted = do
  bestand' <- fileGetPath bestand
  print bestand'
  leegte
verwerkWĳziging toestand bestand _ FileMonitorEventCreated = do
  {- Drie gevallen te verwerken:
       1. Een map Boekerij is aangemaakt -> creëer mapschouwer
       2. Een eboek geplaatst -> toon in overzicht
       3. Anders: Negeer
  -}

  sBestandspad <- fileGetPath bestand
  maybe leegte
        (\pad -> do let bestandsnaam = takeFileName pad
                    --isFolder  <- doesDirectoryExist pad
                    isBestand <- doesFileExist pad

                    -- Geval 1
                    --let isFolder = isFolder && bestandsnaam == "Boekerij"
                    --mits isFolder (putStr "Aanschouw: " >> putStrLn pad >> schouw toestand pad)

                    -- Geval 2
                    mits isBestand $ verwerkBestand pad bestandsnaam
                    leegte
        )
        sBestandspad :: IO ()
 where verwerkBestand pad bestandsnaam = do
        -- Als een oud bestand overschreven wordt, dan creëert GFile.copyFile
        -- een tĳdelĳke stream .goutputstream-... . Deze stream vuurt ook een
        -- FileMonitorEventCreated af die hier wordt afgehandeld, maar we willen
        -- daar niets mee doen, want dat is niet het uiteindelĳke pad naar het
        -- nieuwe bestand. GFile verplaatst daarna nl die stream naar de echte
        -- doellocatie. Die zal een nieuwe event afvuren. Daarom moet een
        -- .goutputstream genegeerd worden
        mits (niet $ "." `isPrefixOf` bestandsnaam) (do
          let verzameling = _boeken toestand
          let overzicht   = _overzicht toestand
          let schep boek = do
               boektoestand <- _boekzichtbouwer toestand boek
               voegIn boektoestand verzameling overzicht
          print pad
          metBoek pad schep)
        leegte
verwerkWĳziging _ _ _ _ = leegte


-- Wisselwerking ---------------------------------------------------------------

integreerBoektuigje :: IORef [Boekzicht] -> FlowBox -> Bestandspad
                    -> Melders -> Boekzicht -> IO ()
integreerBoektuigje verzameling overzicht berging melders boektoestand = do
  let venster = Boekzicht._oervenster boektoestand
  let (Contextmenu _ verwĳ kaftk overh _) = _contextmenu boektoestand

  onButtonReleased verwĳ $ doeBoekverwĳdering boektoestand melders verzameling overzicht
  onButtonReleased kaftk $ doeKaftzoektoning boektoestand melders venster
  onButtonReleased overh $ doeBoekoverheveling boektoestand melders berging
  leegte


doeBoekoverheveling :: Boekzicht -> Melders -> Bestandspad -> IO ()
doeBoekoverheveling boektoestand melders bron = do
  mDoel <- verkrĳgDoelmap (_contextmenu boektoestand) bron
  maybe leegte hevel mDoel
 where hevel doel = do
         let doelT = T.pack doel
         slaagde <- hevelBoekOver (_boek boektoestand) doel
         if slaagde then   _meldOvergehevelde melders (_boek boektoestand) doelT
                    else _meldOnovergehevelde melders (_boek boektoestand) doelT


doeBoekverwĳdering :: Boekzicht -> Melders -> IORef [Boekzicht] -> FlowBox -> IO ()
doeBoekverwĳdering boektoestand melders verzameling overzicht = do
  slaagde <- verwĳderBoek $ _boek boektoestand
  if slaagde then do verwĳder boektoestand verzameling overzicht
                     _meldVerwĳderde melders $ _boek boektoestand
             else _meldOnverwĳderde melders $ _boek boektoestand


doeKaftzoektoning :: Boekzicht -> Melders -> ApplicationWindow -> IO ()
doeKaftzoektoning boektoestand _ venster = toon venster boektoestand


verbindZeef :: Boekenoverzicht -> SearchEntry -> IO ()
verbindZeef toestand zeefinvoer =
  onSearchEntrySearchChanged zeefinvoer opdracht >> leegte
 where opdracht = do naald <- entryGetText zeefinvoer
                     zeef naald (_boeken toestand)

-- Glade -----------------------------------------------------------------------

glade :: BestandspadT
glade = begrMiddelen <> "gtk/boekenoverzicht.ui"

schuiver :: Builder -> IO ScrolledWindow
schuiver = laadGtk "schuiver" ScrolledWindow

boekenoverzicht :: Builder -> IO FlowBox
boekenoverzicht = laadGtk "boekenoverzicht" FlowBox
