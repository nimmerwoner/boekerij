{-# LANGUAGE OverloadedStrings #-}

module Overvenstertje
  ( trachtTeTonen
  ) where


-------------------
--- Eigen steunwaar
-------------------
import           Allerhande
import           AllerhandeGtk
import           Begrippen
import           Hertaling


------------------------
--- Steunwaar van derden
------------------------
import           GI.Gtk


------------------------
--- Eigen datastructuren
------------------------
import OervensterDS


-------
-- Code
-------
trachtTeTonen :: Oervenster -> IO ()
trachtTeTonen toestand = do
  sOver <- overvenstertje =<< bouwerNieuw glade
  case sOver of
    Nothing -> leegte
    Just o  -> do
      windowSetTransientFor o (Just (_oervenster toestand))
      windowSetModal o True
      widgetShowAll o
  

-- Glade -----------------------------------------------------------------------

glade :: BestandspadT
glade = begrMiddelen <> "gtk/overvenstertje.ui"

-- Indien het voorkeurenvenstertje niet geladen kan worden is dat geen reden om
-- het programma te laten crashen. Laad deze daarom veilig
overvenstertje :: Builder -> IO (Soms AboutDialog)
overvenstertje = laadGtkVeilig "overvenstertje" AboutDialog
