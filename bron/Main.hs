{-# LANGUAGE OverloadedStrings, CPP #-}

module Main where


-------------------
--- Eigen steunwaar
-------------------
import Allerhande
import AllerhandeGtk
import Begrippen
import Gettext (bindTekstdomein)
import Hertaling
import Instellingen (Instellingen(..), kaftenfolder, gegevensopslag)
import Koppelaar hiding (Koppelaar)


import qualified Boekzicht       as Bzt (nieuw) -- bedieningslaag
import           Boekenoverzicht hiding (nieuw, Boekenoverzicht(..))
import qualified Boekenoverzicht as Bot (nieuw)
import           Oervenster      hiding (nieuw)
import qualified Oervenster      as Ovr (nieuw, meldOnaangekoppeldToestel,
                                         meldGeslaagdeOverheveling)


------------------------
--- Steunwaar van derden
------------------------
import           Data.Maybe (fromJust)
--import qualified Data.Text as T
import           GI.GdkPixbuf()
import           GI.Gtk hiding (main)
import qualified GI.Gio as Gio
import           System.Directory (createDirectoryIfMissing)


-------
-- Code
-------
main :: IO ()
main = do
  bindTekstdomein begrDnsNaam (Just begrTalen)
  toepassing <- applicationNew (Just begrDnsNaamVeilig)
                               [Gio.ApplicationFlagsFlagsNone]
  Gio.onApplicationActivate toepassing (onthaal toepassing)
  Gio.applicationRun toepassing (Just [begrPrognaamVeiligT])
  return ()


onthaal :: Application -> IO ()
onthaal toepassing = do
  zekerOmgeving

  oervenster  <- Ovr.nieuw
  let venster  = _oervenster oervenster
  applicationAddWindow toepassing venster
  tuigMenuOp oervenster toepassing

  let melders = Melders
                        (const $ putStrLnT "0")
                        (Ovr.meldOnaangekoppeldToestel oervenster)
                        (Ovr.meldGeslaagdeOverheveling oervenster)
                        (\_ _ -> putStrLn "1")
                        (const leegte)
                        (const $ putStrLn "3")

  kaftpixbuf  <- pixbufLaadVanSchĳf begrKafthoogte begrStedehouder
  maybe fout (const leegte) kaftpixbuf
  let schepBzt = Bzt.nieuw venster (fromJust kaftpixbuf) (Just (_toestellen oervenster))
  verzameling <- Bot.nieuw (_hoofdberging $ _instellingen oervenster)
                           melders schepBzt
  plaatsBoekenoverzicht oervenster verzameling begrPrognaamT
  verbindZeef verzameling $ _zeefinvoer oervenster

  let schepperBzt2 = Bzt.nieuw venster (fromJust kaftpixbuf) Nothing
  _     <- Koppelaar.nieuw (_toestellenstapel oervenster)
                           (_toestellen oervenster)
                           (\verz -> do bot <- Bot.nieuw verz melders schepperBzt2
                                        lees bot
                                        verbindZeef bot $ _zeefinvoer oervenster
                                        geef bot)

  verbeidToestellen oervenster melders
  widgetShowAll venster
  lees verzameling
 where fout = putStrLn "Ongeldige stedehouderkaft: Laden mislukt"


-- De toestellenleest (_toestellen::ListStore) en schouwer (_schouwer::Vol.Mon.)
-- zĳn meegegeven gewikkeld in de Oervenster variabele, omdat om onduidelĳke
-- redenen anders de VolumeMonitor-signalen niet vuren.
verbeidToestellen :: Oervenster -> Melders -> IO ()
verbeidToestellen toestand melders = do
  let schouwer = _toestelschouwer toestand

  mounts <- verkrĳgMounts schouwer
  mapM_ (trachtToestelTeKoppelen toestand) mounts

  Gio.onVolumeMonitorMountAdded   schouwer $ trachtToestelTeKoppelen toestand
  Gio.onVolumeMonitorMountRemoved schouwer $ verwĳderToestel toestand

  leegte
 where trachtToestelTeKoppelen toestand mount = do
         geslaagd <- voegToestelToe toestand mount
         if niet geslaagd
         then do naam <- Gio.mountGetName mount
                 _meldOnaangekoppelde melders naam
         else leegte


tuigMenuOp :: Oervenster -> Application -> IO ()
tuigMenuOp toestand toepassing = do
  bouwer <- builderNew
  builderAddFromFile bouwer begrGMenu
  menu <- laadGtkVeilig "hoofdmenu" Gio.MenuModel bouwer

  koppelActie toepassing "voorkeuren"     (const $ toonVoorkeuren toestand)
  koppelActie toepassing "overvenstertje" (const $ toonOver toestand)
  koppelActie toepassing "afsluiting"     (\_ -> Gio.applicationQuit toepassing)

  applicationSetAppMenu toepassing menu
  leegte


zekerOmgeving :: IO ()
zekerOmgeving = do
  kaften   <- kaftenfolder
  gegevens <- gegevensopslag
  createDirectoryIfMissing True kaften
  createDirectoryIfMissing True gegevens
